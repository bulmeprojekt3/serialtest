package me.xkuyax.serialtest;

import com.sun.org.apache.xpath.internal.SourceTree;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.util.Random;
import java.util.Scanner;

public class Bootstrap {

    static SerialPort serialPort;


    //servo values
    // 800 = horizontal
    // 1300 vertikal
    // 1820 horizontal andere seite


    //1460 links
    //1200 ausgangs position
    //900 rechts
    public static void main(String[] args) {
        Random random = new Random();
        serialPort = new SerialPort("COM5");
        try {
            serialPort.openPort();//Open serial port
            serialPort.setParams(SerialPort.BAUDRATE_9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            System.out.println("Starting to listen for inputs!");
            serialPort.addEventListener(new SerialPortReader());
            Scanner scanner = new Scanner(System.in);
            String in;
            while ((in = scanner.nextLine()) != null) {
                if (in.equals("rgb")) {
                    // 1 = blau
                    changeRGB(0, 255, 255, 255);
                    //changeRGB(random.nextInt(4) + 1, random.nextInt(255), random.nextInt(255), random.nextInt(255));
                }
                if (in.equals("vor")) {
                    drive(0, 100, 0, 100);
                }
                if (in.equals("back")) {
                    drive(1, 100, 0, 100);
                }
                if (in.equals("stop")) {
                    drive(0, 0, 0, 100);
                }
                if (in.startsWith("svor")) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (char c : in.toCharArray()) {
                        if (Character.isDigit(c)) {
                            stringBuilder.append(c);
                        }
                    }
                    int i = Integer.parseInt(stringBuilder.toString());
                    servo(1, i);
                }
            }
        } catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }

    private static void servo(int negative, int servo) throws SerialPortException {
        System.out.println("changing to " + servo);
        serialPort.writeIntArray(new int[]{1, 0, 0, negative, servo >> 8, servo & 0xFF});
    }

    private static void drive(int negative, int speed, int servoNegative, int servo) throws SerialPortException {
        serialPort.writeIntArray(new int[]{1, negative, speed, servo >> 8, servo & 0xFF});
    }

    private static void changeRGB(int led, int r, int g, int b) throws SerialPortException {
       //serialPort.writeIntArray(new int[]{1, 1, 100, 1, 100});
        /*int led = buffer[0];
        int r = buffer[1];
        int g = buffer[2];
        int b = buffer[3];*/
        serialPort.writeIntArray(new int[]{0,4,r,g,b});
    }

    private static void sendPacket() {
        System.out.println("Sending a new packet!");
        try {
            serialPort.writeBytes(new byte[]{(byte) 0, (byte) 0xFF, (byte) 0xAA, (byte) 0xBB, (byte) 0xCC, (byte) 0x00});
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    private static class SerialPortReader implements SerialPortEventListener {

        private StringBuilder message = new StringBuilder();

        public void serialEvent(SerialPortEvent event) {
            if (event.isRXCHAR() && event.getEventValue() > 0) {
                try {
                    byte buffer[] = serialPort.readBytes();
                    // System.out.println(new String(buffer));
                    for (byte b : buffer) {
                        message.append((char) b);
                        if (b == '\n') {
                            String msg = message.toString();
                            System.out.print("Arduino: " + msg);
                            message = new StringBuilder();
                        }
                    }
                } catch (SerialPortException ex) {
                    System.out.println(ex);
                    System.out.println("serialEvent");
                }
            }
        }
    }
}
